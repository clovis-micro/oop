<?php

require_once('animal.php');

class Ape extends Frog
{
    public $legs = 2;
    public $yell = "Auooo";
}